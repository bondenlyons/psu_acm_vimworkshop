# PSU ACM Vim Workshop

An interactive workshop to better understand the vim text editor. The material centers around cases which would be useful for writing C/C++ programs using vim. Start by cloning the repo using the terminal command "git clone https://gitlab.com/bondenlyons/psu_acm_vimworkshop". Then open Part 1 by using the terminal command "vim vimWorkShopPt1".
